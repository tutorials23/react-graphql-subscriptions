import React, { useState } from "react";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  gql,
  useMutation,
  useSubscription
} from "@apollo/client";
import { WebSocketLink } from "@apollo/client/link/ws";
import { Container, Row, Col, FormInput, Button } from "shards-react";

const webSocketLink = new WebSocketLink({
  uri: "ws://localhost:4000/",
  options: {
    reconnect: true
  }
});

const client = new ApolloClient({
  link: webSocketLink,
  uri: "http://localhost:4000/",
  cache: new InMemoryCache()
});

const GET_MESSAGES = gql`
  query {
    messages {
      id
      content
      user
    }
  }
`;

const POST_MESSAGE = gql`
  mutation PostMessage($user: String!, $content: String!) {
    postMessage(user: $user, content: $content)
  }
`;

const SUBSCRIBE_MESSAGES = gql`
  subscription {
    messages {
      id
      user
      content
    }
  }
`;

const Messages = ({ user }) => {
  const { data } = useSubscription(SUBSCRIBE_MESSAGES);

  if (!data) {
    return null;
  }

  return (
    <>
      {data.messages.map((msg) => (
        <div
          key={msg.id}
          style={{
            display: "flex",
            justifyContent: user === msg.user ? "flex-end" : "flex-start",
            paddingBottom: "1em"
          }}
        >
          {user !== msg.user && (
            <div
              style={{
                height: 50,
                width: 50,
                marginRight: "0.5em",
                border: "2px solid #e5e6ea",
                borderRadius: "50%",
                fontSize: "18pt",
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <span>{msg.user.slice(0, 2).toUpperCase()}</span>
            </div>
          )}
          <div
            style={{
              background: user === msg.user ? "#58bf56" : "#e5e6ea",
              color: user === msg.user ? "white" : "black",
              padding: "1em",
              borderRadius: "1em",
              maxWidth: "60%"
            }}
          >
            {msg.content}
          </div>
        </div>
      ))}
    </>
  );
};

const Chat = () => {
  const [state, setState] = useState({
    user: "Jack",
    content: ""
  });
  const [postMessage] = useMutation(POST_MESSAGE);

  const onSend = () => {
    if (state.content.length > 0) {
      postMessage({
        variables: {
          user: state.user,
          content: state.content
        }
      });
    }
    setState({
      ...state,
      content: ""
    });
  };

  return (
    <Container>
      <Messages user={state.user} />
      <Row>
        <Col xs={2} style={{ padding: 0 }}>
          <FormInput
            label="User"
            value={state.user}
            onChange={(e) =>
              setState({
                ...state,
                user: e.target.value
              })
            }
          />
        </Col>
        <Col xs={8}>
          <FormInput
            label="Content"
            value={state.content}
            onChange={(e) =>
              setState({
                ...state,
                content: e.target.value
              })
            }
            onKeyUp={(e) => {
              if (e.keyCode === 13) {
                onSend();
              }
            }}
          />
        </Col>
        <Col xs={2} style={{ padding: 0 }}>
          <Button onClick={onSend}>Send</Button>
        </Col>
      </Row>
    </Container>
  );
};

export default () => (
  <ApolloProvider client={client}>
    <Chat />
  </ApolloProvider>
);
